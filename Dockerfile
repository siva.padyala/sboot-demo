# Use an official OpenJDK runtime as a parent image
FROM openjdk:17-jdk-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the executable JAR file into the container
COPY target/sboot-demo-0.0.1-SNAPSHOT.jar /app/sboot-demo-0.0.1-SNAPSHOT.jar

# Expose the port that the application runs on
EXPOSE 8080

# Set the command to run the application
ENTRYPOINT ["java", "-jar", "/app/sboot-demo-0.0.1-SNAPSHOT.jar"]
