package com.spadyala.sboot_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SbootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbootDemoApplication.class, args);
	}

	@GetMapping("/api/hello")
	public ResponseEntity<String> greet(){
		return new ResponseEntity("Welcome to Spring Boot World!!", HttpStatus.OK);
	}
}
